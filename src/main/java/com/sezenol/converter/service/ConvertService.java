/**
 * 
 */
package com.sezenol.converter.service;

/**
 * @author sezenol ConvertService.java Aug 5, 2016
 */
public interface ConvertService {

	/**
	 * @param value
	 * @return Roman version of given integer value
	 */
	public String decimalToRoman(int value);

}
