/**
 * 
 */
package com.sezenol.converter.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sezenol.converter.main.ConverterApplication;

/**
 * @author sezenol ConverterServiceTests.java Aug 10, 2016
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConverterApplication.class)
public class ConverterServiceTests {

	@Autowired
	private ConvertService convertService;

	@Test
	public void convert555toRoman() {

		int value = 555;
		String expectedValue = "DLV";

		Assert.assertEquals(convertService.decimalToRoman(value), expectedValue);

	}

	@Test
	public void convert1111toRoman() {

		int value = 1111;
		String expectedValue = "MCXI";

		Assert.assertEquals(convertService.decimalToRoman(value), expectedValue);

	}

}
