/**
 * 
 */
package com.sezenol.converter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sezenol.converter.exception.enums.ErrorCodes;

/**
 * @author sezenol InternalServerException.java Aug 23, 2016
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal server error")
public class InternalServerException extends ConverterException {

	private static final long serialVersionUID = 1L;

	public InternalServerException(String message) {
		super(ErrorCodes.INTERNAL_SERVER_ERROR, message);
	}

}
