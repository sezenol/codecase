/**
 * 
 */
package com.sezenol.converter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sezenol.converter.exception.InternalServerException;
import com.sezenol.converter.exception.InvalidRequestParameterException;
import com.sezenol.converter.model.RomanNumber;
import com.sezenol.converter.service.ConvertService;

/**
 * @author sezenol ConverterController.java Aug 5, 2016
 */
@RestController
public class ConverterController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConverterController.class);

	@Autowired
	ConvertService convertService;

	@RequestMapping(value = "convertToRomanNumerals", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RomanNumber> test(@RequestParam(required = true) String value) {

		LOGGER.debug("Received value is {}", value);

		int val;
		try {
			val = Integer.parseInt(value);
		} catch (NumberFormatException e) {
			LOGGER.error("{} is not a number", value);
			throw new InvalidRequestParameterException(e.getMessage());
		}

		String convertedValue;
		try {
			convertedValue = convertService.decimalToRoman(val);
		} catch (Exception e) {
			LOGGER.error("Internal error while converting", e);
			throw new InternalServerException(e.getMessage());
		}

		RomanNumber romanNumber = new RomanNumber();
		romanNumber.setValue(convertedValue);

		return new ResponseEntity<RomanNumber>(romanNumber, HttpStatus.OK);
	}

}
