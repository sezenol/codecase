/**
 * 
 */
package com.sezenol.converter.service;

/**
 * @author sezenol RomanNumberService.java Aug 7, 2016
 */
public interface RomanNumberService {

	public String toRoman(int number);
}
