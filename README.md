# Roman Numeral Converter

This project aims to convert given an integer value to Roman numeral representation

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisities

1- Java 1.8
2- Maven 3+


### Installing

Run the following commands step by step for installation
1- cd {project-path}
2- mvn install
3- {jdk-1.8-path}/bin/java -jar {project-path}/target/converter-0.0.1.jar
4- open localhost:8080/index.html

## Running the tests

Run the following commands to execute automated tests

1- cd {project-path}
2- mvn test



## Versioning

first version 0.0.1

## Authors

* **Mahmut Sezenol** - *Initial work* 


