/**
 * 
 */
package com.sezenol.converter.exception.enums;

/**
 * @author sezenol
 * ErrorCodes.java Aug 9, 2016 
 */
public enum ErrorCodes {
	INTERNAL_SERVER_ERROR(1000),
    ERR_INVALID_REQUEST_PARAMETERS(1001);
    
    private int code;

    ErrorCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
