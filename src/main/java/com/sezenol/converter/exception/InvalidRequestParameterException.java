/**
 * 
 */
package com.sezenol.converter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sezenol.converter.exception.enums.ErrorCodes;

/**
 * @author sezenol InvalidRequestParameterException.java Aug 9, 2016
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request parameters")
public class InvalidRequestParameterException extends ConverterException {

	private static final long serialVersionUID = 1L;

	public InvalidRequestParameterException(String message) {
		super(ErrorCodes.ERR_INVALID_REQUEST_PARAMETERS, message);
	}

}
