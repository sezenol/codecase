/**
 * 
 */
package com.sezenol.converter.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sezenol.converter.service.ConvertService;
import com.sezenol.converter.service.RomanNumberService;

/**
 * This service can offer several conversation method
 * But now it has only Roman numeral conversion
 * 
 * @author sezenol ConverServiceImpl.java Aug 7, 2016
 */
@Service
public class ConvertServiceImpl implements ConvertService {

	@Autowired
	private RomanNumberService romanNumberService;

	/*
	 * Convert to Roman representation of given integer value
	 * @param value integer value
	 * @return Roman Numeral ex. V, MM, MXI...
	 * 
	 * @see com.sezenol.converter.service.ConvertService#decimalToRoman(int)
	 */
	@Override
	public String decimalToRoman(int value) {
		return romanNumberService.toRoman(value);
	}

}
