var converterModule = angular.module('converterApp', [ 'ngAnimate' ]);

converterModule.controller('converterController', function($scope, $http) {

	var urlBase = "";
	$scope.toggle = true;
	$scope.selection = [];
	$http.defaults.headers.post["Content-Type"] = "application/json";

	$scope.convert = function convert() {
		if ($scope.number == "") {
			alert("Insufficient Data! Please provide an integer value!");
		} else {
			$http.get(
					urlBase + '/convertToRomanNumerals?value=' + $scope.number,
					{}).success(function(data, status, headers) {
				$scope.roman = data.value;
			}).error(function(data, status, headers) {
				$scope.roman = data.message;
			});
		}
	};

});
