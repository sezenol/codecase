/**
 * 
 */
package com.sezenol.converter.exception;

import com.sezenol.converter.exception.enums.ErrorCodes;

/**
 * @author sezenol ConverterException.java Aug 9, 2016
 */
public class ConverterException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private int code;
	private String exceptionMessage;

	public ConverterException(ErrorCodes code) {
		this.code = code.getCode();
	}

	public ConverterException(ErrorCodes code, String message) {
		this(code);
		this.exceptionMessage = message;
	}

	public int getCode() {
		return code;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}
}
