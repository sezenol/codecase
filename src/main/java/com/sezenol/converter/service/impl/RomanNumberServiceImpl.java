/**
 * 
 */
package com.sezenol.converter.service.impl;

import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.sezenol.converter.service.RomanNumberService;

/**
 * @author sezenol RomanNumberServiceImpl.java Aug 7, 2016
 */
@Component
public class RomanNumberServiceImpl implements RomanNumberService {

	private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

	@PostConstruct
	public void init() {
		map.put(1000, "M");
		map.put(900, "CM");
		map.put(500, "D");
		map.put(400, "CD");
		map.put(100, "C");
		map.put(90, "XC");
		map.put(50, "L");
		map.put(40, "XL");
		map.put(10, "X");
		map.put(9, "IX");
		map.put(5, "V");
		map.put(4, "IV");
		map.put(1, "I");
	}

	/*
	 * Converts the given integer value to Roman representation
	 * 
	 * @param number is to be converted value
	 * 
	 * returns Roman representation
	 * 
	 * @see com.sezenol.converter.service.RomanNumberService#toRoman(int)
	 */
	@Override
	public String toRoman(int number) {
		// TODO : improve for stackoverflow exception, can be endless loop
		int l = map.floorKey(number);
		if (number == l) {
			return map.get(number);
		}
		return map.get(l) + toRoman(number - l);
	}

}
