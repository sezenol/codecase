/**
 * 
 */
package com.sezenol.converter.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.sezenol.converter.main.ConverterApplication;
import com.sezenol.converter.model.RomanNumber;
import com.sezenol.converter.service.ConvertService;

/**
 * @author sezenol ConverterControllerUnitTests.java Aug 9, 2016
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = ConverterApplication.class)
public class ConverterControllerUnitTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@MockBean
	ConvertService convertService;

	@Before
	public void setUp() {
		given(this.convertService.decimalToRoman(5)).willReturn("5");
		given(this.convertService.decimalToRoman(555)).willReturn("DLV");
		given(this.convertService.decimalToRoman(1111)).willReturn("MCXI");
	}

	@Test
	public void convert5toRoman() {

		RomanNumber expected = new RomanNumber();
		expected.setValue("5");
		RomanNumber response = this.restTemplate.getForEntity("/convertToRomanNumerals?value={value}", RomanNumber.class, 5).getBody();
		assertThat(response, is(expected));
	}

	@Test
	public void convert555toRoman() {

		RomanNumber expected = new RomanNumber();
		expected.setValue("DLV");
		RomanNumber response = this.restTemplate.getForEntity("/convertToRomanNumerals?value={value}", RomanNumber.class, 555).getBody();
		assertThat(response, is(expected));
	}

	@Test
	public void convert1111toRoman() {

		RomanNumber expected = new RomanNumber();
		expected.setValue("MCXI");
		RomanNumber response = this.restTemplate.getForEntity("/convertToRomanNumerals?value={value}", RomanNumber.class, 1111).getBody();
		assertThat(response, is(expected));
	}
	
	
	
}