/**
 * 
 */
package com.sezenol.converter.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * This class is used for encapsulation of Roman values
 * 
 * @author sezenol RomanNumber.java Aug 5, 2016
 */
public class RomanNumber implements Serializable {

	/**
	 * Generated Serial Version ID
	 */
	private static final long serialVersionUID = 3480574128168458989L;

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * Compare two objects based on values
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null)
			return false;

		if (obj instanceof RomanNumber)
			return StringUtils.equals(this.getValue(), ((RomanNumber) obj).getValue());

		return false;
	}

}
